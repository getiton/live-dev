# Live-Reload &amp; Live-Browser-Sync Express Development Scaffolding

## About

This is an [_Express_](https://expressjs.com) scaffolding project for developers.  Starting with the default _Express_ scaffolding, _Node_ modules were added to enable

+   __live-browser-sync__ &mdash; content auto-refresh in browser whenever changes are made in the front-end; and
+   __live-reload__ &mdash; auto-reload server whenever changes are made in the back-end codes.

The setup basically:

+   scaffolds with a default _Express_ web-app project,
+   adds browser refresh on change using `livereload`,
+   adds server restart on change using `nodemon`, and
+   adds `connect-livereload` to enable browser (one or more clients) to poll over a second connection so as to listen for signal from server to refresh.

These setup steps were originally given in the awesome [Blog](https://bytearcher.com/articles/refresh-changes-browser-express-livereload-nodemon/) by Panu Pitkamaki and his crystal-clear [Walk-through](https://www.youtube.com/watch?v=P4RWMZA4yJo) on Youtube.

<br>

## Status

This scaffolding is ready to be used.

The [Step-by-Step](//getiton.gitlab.io/live-dev/steps) page with detailed instructions on how this scaffolding was created is still a work-in-progress.

<br>

## Target Product

This repository is an _Express_ project scaffolding for developers.  It is pre-set with server live-reload and live-browser-sync features so that visual and behavioural changes during development are automatically updated in the browser view.  This features eliminate the need to turn your attention away to-and-from editing to restart the _Express_ server and hit refresh on the browser after every changes while developing your web-app project.

<br>

## Pre-requisites (programs to be Installed) on Your Computer

+   a modern browser like _Firefox_ or _Vivaldi_,
+   a code editor like _Visual Studio Code_ or _Sublime-Text_,
+   a terminal window, like `xterm`,
+   _Git_ (version 2.33.0 was used),
+   _Node_ (version 12.22.4 was used),
+   `yarn` (version 1.22.11 was used), or `npm` to manage _Node_ modules.

<br>

## Getting this Scaffolding for Your Own Project

1.  On your computer, create a parent folder to place this scaffolding.

    ```sh
    mkdir my-projects
    cd my-projects
    ```

2.  Clone this scaffolding inside the parent folder,

    ```sh
    git clone https://gitlab.com/getiton/live-dev.git
    ```

3.  **And** change into the **project** folder,

    ```sh
    cd live-dev
    ```

4.  Install the dependency modules in `package.json`,

    ```sh
    yarn install
    ```

    or

    ```sh
    npm install
    ```

5.  If you have not done so, go ahead and install _Nodemon_ module,

    ```sh
    yarn global add nodemon
    ```

    or

    ```sh
    npm --global install nodemon
    ```

    Note: _Nodemon_ is usually installed as a global module.

6.  The scaffolding is now ready.  Test it by starting the _Express_ server like so, `yarn watch` or `npm run watch`.

    Next, open your favourite browser and point the URL to <http://localhost:3000>. The web-app should now appear in your browser.

    With your favourite code editor, make some changes to any of the _watched_ files and see the magic happens &mdash; **live-server-reload** and **live-browser-refresh** in action!  No more update command while you focus on creating your amazing app.

<br>

## Git Repository Tracking Remotely and Locally

Suppose you want to use this scaffolding for your own project, there's no need to keep _Git_
tracking (continuous update with `git push|pull`) this scaffolding on _Gitlab_.  There are two simple ways to go about resolving this:

1.  Use _Git_ to continue tracking locally only, or
2.  Use _Git_ to continue tracking locally and in your remote repository.

Before we begin, let take a look at your _Git_ configuration by this command in your project folder.

```sh
git config --list
```

It should show something like this,

```output
user.name=*your name*
user.email=*your email*
init.defaultbranch=master
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=https://gitlab.com/getiton/live-dev.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.main.remote=origin
branch.main.merge=refs/heads/main
```

And the file `.git/config` has,

```output
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
[remote "origin"]
        url = https://gitlab.com/getiton/live-dev.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "main"]
        remote = origin
        merge = refs/heads/main
```

### 1. Continue tracking locally

Remote tracking can be removed with the following commands:

```sh
git branch --unset-upstream main
git remote remove origin
```

Now `.git/config` is left with,

```output
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
```

It might be easier to use a code-editor and manually comment-out the last six lines.  This leaves an option to uncomment them whenever they are needed later.

### 2. Continue tracking locally and remotely

Alternatively, if you intend to track your project with your own remote repository, issue these command instead of those above.

```sh
git remote set-url origin <your-git-repository>.git
```

where `<your-git-repository>.git` can be found from your favourite repository provider, like _bitBucket_, _GitLab_ or _Github_.  Refer to [StackOverflow](https://stackoverflow.com/questions/2432764/how-to-change-the-uri-url-for-a-remote-git-repository) for more options.

To verify,

```sh
git remote -v
```

Make some changes and update your remote repository (for the first time) with,

```sh
git push --set-upstream origin main
```

and subsequently with `git push`.


<br>

---

<center>~ F i n ~</center>
