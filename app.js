var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// 2021Sep23
// Use livereload module to start another server to notify browser on front-end changes.
// Use connect-livereload lib to add scripts to every front-end pages to poll livereload server.
// Allow browser to refresh on changes to front-end assets.
// [https://bytearcher.com/articles/refresh-changes-browser-express-livereload-nodemon/]
var livereload = require('livereload');
var connectLivereload = require('connect-livereload');

const publicDir = path.join(__dirname, 'public');

var liveReloadServer = livereload.createServer();
liveReloadServer.watch(publicDir);
// Next line is required to trigger when dynamic pages change.
// Simply do liveReloadServer.refresh("/"); won't work because
// the server is also being reloaded on change and will not be
// ready to serve anything.
// Solution: Ask client to
// a) listen for event "connection"
// b) wait 100 ms.
// c) Do this ONCE on every server starts.
liveReloadServer.server.once("connection", () => {
  setTimeout(() => {
    liveReloadServer.refresh("/");
  }, 10);
});

var indexRouter = require('./routes/index');
var stepsRouter = require('./routes/steps');
var usersRouter = require('./routes/users');

var app = express();

// 2021Sep23
// Make app aware of (calls) the connectLivereload() service.
app.use(connectLivereload());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(publicDir));

app.use('/', indexRouter);
app.use('/syntax', indexRouter);
app.use('/steps', stepsRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
