var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('steps', {
    category: 'ExpressJs Exercise',
    title: 'Server Live-Reload & Browser Live-Sync'
  });
});

module.exports = router;
