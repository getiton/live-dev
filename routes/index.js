var express = require('express');
var router = express.Router();

/* GET / of '/steps' route */
router.get('/', function(req, res, next) {
  res.render('index', {
    category: 'ExpressJs Exercise',
    title: 'Live-Reload & Live-Browser-Sync for ExpressJs Developers'
  });
});

router.get('/jade-syntax', function(req, res, next) {
  res.render('syntax', {
    category: 'ExpressJs Exercise',
    title: 'Jade Template Syntax',
    my_var: {name: 'Jade', type: 'templating engine'}
  });
});


module.exports = router;
